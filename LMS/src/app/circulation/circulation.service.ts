import { Injectable } from '@angular/core';
import { Issue } from './issue';
import { ApphttpclientService } from './../apphttpclient.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CirculationService {

  issues : Issue[] = [];
  private issueDataSubject = new BehaviorSubject<Issue[]>([]);

  constructor(private httpClientService : ApphttpclientService) { 
    this.initIssuesData();
  }

  initIssuesData(){
    this.getAllIssues(true);
  }

  getAllIssues(flag : boolean){
    if(flag || !this.issueDataSubject.getValue()){
      this.httpClientService.get("issues").subscribe((res:Issue[]) => {
          this.issueDataSubject.next(res);
    });
   }
      return this.issueDataSubject.asObservable();
  }

}
