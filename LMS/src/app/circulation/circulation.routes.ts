import {RouterModule, Routes} from '@angular/router';
import { CirculationListComponent } from './circulation-list/circulation-list.component';

const cirulationRoutes : Routes =[
    {
        path : '',
        redirectTo : 'circulation-list',
        pathMatch : 'full'
    },
    {
        path : 'circulation-list',
        component : CirculationListComponent
    }
];

export const CirculationModuleRoutes = RouterModule.forChild(cirulationRoutes);