import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CirculationListComponent } from './circulation-list/circulation-list.component';
import { RouterModule } from '@angular/router';
import { CirculationModuleRoutes } from './circulation.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [CirculationListComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    CirculationModuleRoutes
  ]
})
export class CirculationModule { }
