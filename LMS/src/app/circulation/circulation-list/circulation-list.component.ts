import { Component, OnInit } from '@angular/core';
import { Issue } from '../issue';
import { CirculationService } from '../circulation.service';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import { AppToasterService } from 'src/app/app-toaster.service';
import { LoaderService } from 'src/app/loader.service';

@Component({
  selector: 'app-circulation-list',
  templateUrl: './circulation-list.component.html',
  styleUrls: ['./circulation-list.component.css']
})
export class CirculationListComponent implements OnInit {
  issueColumns = [ "ISSUE ID", "USER ID", "BOOK ID"];
  actionColumns = [ "# EDIT" , "# DELETE"];
  tableHeading = "Issues Table";
  issues : Issue[] = [];
  keys : string[] = ["issueId",  "userId", "bookId"];
  searchText : string = "";
  selectedIssue : Issue ;
  isEdit : boolean = false;
  dialogTitle : string ;
  searchColumns : string [] = ["issueId", "userId"];

  constructor(private circulationService : CirculationService, private httpClientService : ApphttpclientService,
    private toastr : AppToasterService, private loader : LoaderService) {
    this.selectedIssue = new Issue();
   }

   ngOnInit() {
    this.loader.loaderStart();
    this.circulationService.getAllIssues(false).subscribe(res => {
      this.issues = res;
      this.loader.loaderEnd();
    });

  }
  delete(issue : Issue){
    this.httpClientService.delete("/users/"+issue.userId+"/books/"+issue.bookId).subscribe(res=>{
      console.log("Issue deleted");
      this.circulationService.getAllIssues(true);
      this.toastr.success("Issues Deleted Successfully !");
    });
  }

}
