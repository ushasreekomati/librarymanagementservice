import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { RouterModule } from '@angular/router';
import { TableComponent } from './common/table/table.component';
import { SearchPipe } from './common/search.pipe';



@NgModule({
  declarations: [HeaderComponent, FooterComponent, TableComponent, SearchPipe],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports : [HeaderComponent, FooterComponent, TableComponent,SearchPipe]
})
export class SharedModule { }
