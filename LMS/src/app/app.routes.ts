import { Routes, RouterModule } from "@angular/router";

const appRoutes : Routes = [
    {
        path : '',
        redirectTo : 'books',
        pathMatch: 'full'
    },
    // {
    //     path:'prelogin',
    //     loadChildren : './prelogin/prelogin.module#PreloginModule'        
    // },
    {
        path : 'books',
        loadChildren : './books/books.module#BooksModule',
    },
    {
        path : 'users',
        loadChildren : './users/users.module#UsersModule',
        

    },
    {
        path : 'circulations',
        loadChildren : './circulation/circulation.module#CirculationModule',
        
    }
];

export const appModuleRoutes = RouterModule.forRoot(appRoutes);