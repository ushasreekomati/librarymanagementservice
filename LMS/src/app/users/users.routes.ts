import {RouterModule, Routes} from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';

const userRoutes : Routes = [
    {
        path : '',
        redirectTo : 'user-list',
        pathMatch : 'full'
    },
    {
        path : 'user-list',
        component : UsersListComponent
    }
];

export const UserRoutesModule = RouterModule.forChild(userRoutes);