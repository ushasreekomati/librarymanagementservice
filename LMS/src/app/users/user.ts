
export class User {
    public userId : number;
    public name : string;
    public emailId : string;
    public password : string;
    public role : string;
}