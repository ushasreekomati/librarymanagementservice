import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import { AppToasterService } from 'src/app/app-toaster.service';
import { UserService } from '../user.service';
import { LoaderService } from 'src/app/loader.service';
import { Issue } from 'src/app/circulation/issue';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  
  userColumns = [ "ID", "USER NAME", "EMAIL ID","ISSUE BOOK","VIEW ISSUES"];
  actionColumns = [ "# EDIT" , "# DELETE"];
  tableHeading = "Users Table";
  users : User[] = [];
  keys : string[] = ["userId",  "name", "emailId"];
  searchText : string = "";
  selectedUser : User ;
  isEdit : boolean = false;
  dialogTitle : string ;
  searchColumns : string [] = ["userId", "name"];
  userId :number;
  userIdForIssue :number;
  issues : Issue[];
  private userIssueDataSubject = new BehaviorSubject<Issue[]>([]);

  @Output()
  saveEvent = new EventEmitter<any>();

  @Input()
  bookIdToIssue : number;
  
  constructor(private userService : UserService, private httpClientService : ApphttpclientService,
    private toastr : AppToasterService, private loader : LoaderService) {
    this.selectedUser = new User();
   }
  ngOnInit() {
    this.loader.loaderStart();
    this.userService.getAllUsers(false).subscribe(res => {
      this.users = res;
      this.loader.loaderEnd();
    });
  }
  delete(user : User){
    this.httpClientService.delete("users/"+user.userId).subscribe(res=>{
      console.log("User deleted");
      this.userService.getAllUsers(true);
      this.toastr.success("User Deleted Successfully !");
    });
  }
  create(){
    console.log("create user in UserList");
    this.selectedUser = new User();
    this.isEdit = false;
    this.dialogTitle = "Add User";
  }
  getUserId(user : User){
    console.log("create user in UserList");
    this.userId = user.userId;
    
  }

  update(user : User){
    console.log("update user in UserList");
    this.selectedUser = user;
    this.isEdit = true;
    this.dialogTitle = "Edit User";
  }

 saveUserAck(event){
    document.getElementById("closePopup").click();
    console.log("--"+event);
  }

  issueBook(bookId :number){
    console.log("Create Issue for User"+this.userId);
      this.httpClientService.post("users/"+this.userId+"/books/"+this.bookIdToIssue,"").subscribe(res => {
        console.log("User created");
        this.saveEvent.emit("success");
        this.toastr.success("User Added Successfully !");
        this.userService.getAllUsers(true);
      });
  }
  getIssuesOfUser(user : User)
  {
    console.log("Get Issue for User "+user.userId);
    this.userService.getIssuesOfUser(user).subscribe((result :Issue[])=> {
      this.issues= result;
    }
      );
     return this.issues;

  }

}
