import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list/users-list.component';
import {RouterModule} from '@angular/router';
import { UserRoutesModule } from './users.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { UserComponent } from './user/user.component';
import { UsersIssuesComponent } from './users-issues/users-issues.component';



@NgModule({
  declarations: [UsersListComponent, UserComponent, UsersIssuesComponent],
  imports: [
    CommonModule,RouterModule,SharedModule,
    FormsModule,UserRoutesModule
  ]
})
export class UsersModule { }
