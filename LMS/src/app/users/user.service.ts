import { Injectable } from '@angular/core';
import { User } from './user';
import { ApphttpclientService } from './../apphttpclient.service';
import { BehaviorSubject } from 'rxjs';
import { Issue } from '../circulation/issue';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users : User[] = [];
  private userDataSubject = new BehaviorSubject<User[]>([]);
  private userIssueDataSubject = new BehaviorSubject<Issue[]>([]);

  user :User;

  constructor(private httpClientService : ApphttpclientService) { 
    this.initUsersData();
  }
  initUsersData(){
    this.getAllUsers(true);
  }

  getAllUsers(flag : boolean){
    if(flag || !this.userDataSubject.getValue()){
      this.httpClientService.get('users').subscribe((res:User[]) => {
          this.userDataSubject.next(res);
    });
   }
      return this.userDataSubject.asObservable();
  }

  getIssuesOfUser(user : User)
  {
    console.log("Issues for User "+user.userId);
      this.httpClientService.get("users/"+user.userId+"/books").subscribe((res:Issue[]) => {
        this.userIssueDataSubject.next(res);      
    });
    return this.userIssueDataSubject.asObservable();
  }
  getUserByUserId(userId :number)
  {
    console.log("User for User "+userId);
      this.httpClientService.get("users/"+ userId).subscribe((res:User) => {
        this.user= res;    
    });
    return this.user;
  }


}
