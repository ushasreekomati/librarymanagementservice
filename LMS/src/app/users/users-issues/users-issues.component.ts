import { Component, OnInit, Input, Output , EventEmitter} from '@angular/core';
import { Issue } from 'src/app/circulation/issue';
import { User } from '../user';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import { AppToasterService } from 'src/app/app-toaster.service';
import { LoaderService } from 'src/app/loader.service';
import { BehaviorSubject } from 'rxjs';
import { UserService } from '../user.service';

@Component({
  selector: 'app-users-issues',
  templateUrl: './users-issues.component.html',
  styleUrls: ['./users-issues.component.css']
})
export class UsersIssuesComponent implements OnInit {

  issueColumns = [ "ISSUE ID", "USER ID","BOOK ID"];
  actionColumns = [  "# DELETE"];
  tableHeading = "Issues of User";
  issues : Issue[] = [];
  keys : string[] = ["issueId",  "userId", "bookId"];
  selectedIssue : Issue  ;
  dialogTitle : string ;
  userId :number;
  userIdForIssue :number;
  issuesOfUser : Issue[];
  private userIssueDataSubject = new BehaviorSubject<Issue[]>([]);

  @Input()
  user : User;

  @Output()
  saveEvent = new EventEmitter<any>();

  // @Input()
  // issues : Issue[] = [];

  constructor(private userService : UserService, private httpClientService : ApphttpclientService,
    private toastr : AppToasterService, private loader : LoaderService) {
    this.selectedIssue = new Issue();
   }

  ngOnInit() {    
    console.log("User "+ this.user.userId);
    // this.loader.loaderStart();
    // this.userService.getIssuesOfUser(this.user).subscribe(res => {
    //   this.issues = res;
    //   this.loader.loaderEnd();
    // });

    this.getIssuesOfUser(this.user);

  }

  getIssuesOfUser(userr :User)
  {
    this.loader.loaderStart();
    this.userService.getIssuesOfUser(userr).subscribe(res => {
      this.issues = res;
      this.loader.loaderEnd();
  });
}
  getIssuesOfUserById(userId :number)
  {
    this.loader.loaderStart();
    
    this.userService.getIssuesOfUser(this.userService.getUserByUserId(userId)).subscribe(res => {
      this.issues = res;
      this.loader.loaderEnd();
  });
 }
  delete(issue : Issue){
    this.httpClientService.delete("users/"+issue.userId+"/books/"+issue.bookId).subscribe(res=>{
      console.log("Issue deleted");
      this.saveEvent.emit("success");
      this.toastr.success("Issue Deleted Successfully !");
    });
    this.getIssuesOfUser(this.user);
  }

}
