import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppToasterService } from 'src/app/app-toaster.service';
import { LoaderService } from 'src/app/loader.service';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import { User } from '../user';
import { UserService } from '../user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input()
  isEdit : boolean = false;

  @Input()
  user : User;

  @Output()
  saveEvent = new EventEmitter<any>();


  constructor(private httpClientService : ApphttpclientService, private loader :LoaderService,
    private userService : UserService, private toastr : AppToasterService) {
   }

  ngOnInit() {
  }

  
  saveUser(){
    this.loader.loaderStart();
    if(this.isEdit){
      console.log("Edit User");
      this.httpClientService.put("users/"+this.user.userId, this.user).subscribe(res => {
        console.log("User is updated");
        this.saveEvent.emit("success");
        this.toastr.success("User Edited Successfully !");
        this.userService.getAllUsers(true);
      });
    }
    else {
      console.log("Create User");
      this.httpClientService.post("users", this.user).subscribe(res => {
        console.log("User created");
        this.saveEvent.emit("success");
        this.toastr.success("User Added Successfully !");
        this.userService.getAllUsers(true);
      });
    }
this.loader.loaderEnd();
  }

}
