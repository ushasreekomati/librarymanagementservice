import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { appModuleRoutes } from './app.routes';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule,
    BrowserModule,
    SharedModule,
    HttpClientModule,
    appModuleRoutes,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut : 1000,
      positionClass : 'toast-top-right',
      preventDuplicates : false
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
