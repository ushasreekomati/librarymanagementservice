import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BooksService } from '../books.service';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import { AppToasterService } from 'src/app/app-toaster.service';
import { LoaderService } from 'src/app/loader.service';

@Component({
  selector: 'app-bookslist',
  templateUrl: './bookslist.component.html',
  styleUrls: ['./bookslist.component.css']
})
export class BookslistComponent implements OnInit {


  bookColumns = [ "ID", "BOOK NAME", "GENRE", "AUTHOR"];
  actionColumns = [ "# EDIT" , "# DELETE"];
  tableHeading = "Books Table";
  books : Book[] = [];
  keys : string[] = ["id",  "title", "genre","author"];
  searchText : string = "";
  selectedBook : Book ;
  isEdit : boolean = false;
  dialogTitle : string ;
  searchColumns : string [] = ["id", "title"];

  constructor(private bookService : BooksService, private httpClientService : ApphttpclientService,
    private toastr : AppToasterService, private loader : LoaderService) {
    this.selectedBook = new Book();
   }


  ngOnInit() {
    this.loader.loaderStart();
    this.bookService.getAllBooks(false).subscribe(res => {
      this.books = res;
      this.loader.loaderEnd();
    });
  }

  delete(book : Book){
    this.httpClientService.delete("books/"+book.id).subscribe(res=>{
      console.log("book deleted");
      this.bookService.getAllBooks(true);
      this.toastr.success("Book Deleted Successfully !");
    });
  }

  create(){
    console.log("create book in BookList");
    this.selectedBook = new Book();
    this.isEdit = false;
    this.dialogTitle = "Add Book";
  }

  update(book : Book){
    console.log("update book in BookList");
    this.selectedBook = book;
    this.isEdit = true;
    this.dialogTitle = "Edit Book";
  }

 saveBookAck(event){
    document.getElementById("closePopup").click();
    console.log("--"+event);
  }

}
