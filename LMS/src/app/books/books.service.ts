import { Injectable } from '@angular/core';
import { ApphttpclientService } from './../apphttpclient.service';
import { Book } from './book';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books : Book[] = [];
  private bookDataSubject = new BehaviorSubject<Book[]>([]);

  constructor(private httpClientService : ApphttpclientService) { 
    this.initBooksData();
  }

  initBooksData(){
    this.getAllBooks(true);
  }

  getAllBooks(flag : boolean){
    if(flag || !this.bookDataSubject.getValue()){
      this.httpClientService.get('books').subscribe((res:Book[]) => {
          this.bookDataSubject.next(res);
    });
   }
      return this.bookDataSubject.asObservable();
  }

}
