import { BookStatus } from './book.enum';

export class Book {
    public id : number;
    public title : string;
    public genre : string;
    public author : string;
    
}