import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../book';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import { BooksService } from '../books.service';
import { AppToasterService } from 'src/app/app-toaster.service';
import { LoaderService } from 'src/app/loader.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
 
  @Input()
  isEdit : boolean = false;

  @Input()
  book : Book;

  @Output()
  saveEvent = new EventEmitter<any>();

  constructor(private httpClientService : ApphttpclientService, private loader :LoaderService,
    private bookService : BooksService, private toastr : AppToasterService) {
   }
  
  ngOnInit() {
  }

  saveBook(){
    this.loader.loaderStart();
    if(this.isEdit){
      console.log("Edit book");
      this.httpClientService.put("books/"+this.book.id, this.book).subscribe(res => {
        console.log("book updated");
        this.saveEvent.emit("success");
        this.toastr.success("Book Eidted Successfully !");
        this.bookService.getAllBooks(true);
      });
    }
    else {
      console.log("Create book");
      this.httpClientService.post("books", this.book).subscribe(res => {
        console.log("book created");
        this.saveEvent.emit("success");
        this.toastr.success("Book Added Successfully !");
        this.bookService.getAllBooks(true);
      });
    }
this.loader.loaderEnd();
  }
}
