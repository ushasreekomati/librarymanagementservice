export enum BookStatus {   
    AVAILABLE = "AVAILABLE",
    REQUESTED = "REQUESTED",
    ISSUED = "ISSUED"
}