import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookComponent } from './book/book.component';
import { BookslistComponent } from './bookslist/bookslist.component';
import { RouterModule } from '@angular/router';
import { booksModuleRoutes } from './books.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { BooksService } from './books.service';

@NgModule({
  declarations: [BookComponent, BookslistComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    booksModuleRoutes
  ],
  providers : []
})
export class BooksModule { }
