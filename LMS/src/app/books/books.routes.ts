import { Routes, RouterModule } from "@angular/router";
import { BookslistComponent } from './bookslist/bookslist.component';

const booksRoutes : Routes = [
    {
        path : '',
        redirectTo : 'booklist',
        pathMatch: 'full'
    },
    {
        path:'booklist',
        component : BookslistComponent
    }
];

export const booksModuleRoutes = RouterModule.forChild(booksRoutes);