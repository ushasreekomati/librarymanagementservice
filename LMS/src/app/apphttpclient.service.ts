import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApphttpclientService {

  domainURL = environment.baseURI;
  constructor(private _httpClient : HttpClient) { }

  get(endpoint){
    return this._httpClient.get(this.domainURL+endpoint);
  }

  post(endpoint, bodyParams){
    return this._httpClient.post(this.domainURL+endpoint, bodyParams);
  }

  put(endpoint, bodyParams){
    return this._httpClient.put(this.domainURL+endpoint, bodyParams);
  }

  delete(endpoint){
    return this._httpClient.delete(this.domainURL+endpoint);
  }
}
