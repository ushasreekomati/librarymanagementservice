# LMSA8

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.0.

## NodeModule Install

Go the Project Root directory, Run `npm install` to download and install the required dependencies to the project.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


Steps :
create project //ng new <proj_name>
install bootstrap //npm install --save bootstrap
install jquery //npm install --save jquery
intall font-awesome // npm install --save font-awesome

//shared module [Parent -Child module interaction]
> 1. Create shared module //ng g m shared
> 2. Create header component //ng g c shared/common/header
> 3. Create footer component //ng g c shared/common/footer

//prelogin module [Load Child Module through routing] //Routing concepts, data-binding, form-validation
> 1. Create prelogin module //ng g m prelogin
> 2. Create login component //ng g c prelogin/login
> 3. Create register component //ng g c prelogin/register
[ngForm, ngModel, ngSubmit]

//books module [HttpModule, CRUD ops]
> 1. Create books module //ng g m books
> 2. Create books component //ng g c books/books --flat=true
> 3. Create bookslist component // ng g c bookslist
> 4. Create book component // ng g c book

